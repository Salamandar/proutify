# Proutify
Remplace le nom des membres du gouvernement par leurs versions proutifiées.
Adapté du super [Manu Auto Correct](https://github.com/Bo-Duke/Manu-Auto-Correct).

## Comment l'installer sur mon navigateur?
## Firefox
Télécharger l'extension directement depuis [le magasin Mozilla](https://addons.mozilla.org/fr/firefox/addon/proutify/) !

(Si ce lien n'est pas disponible, installez l'extension à partir du fichier : [téléchargez le fichier `.xpi`](https://github.com/banality13/proutify/releases/download/v1.2/proutify-1.2-fx.xpi) et ouvrez-le avec votre navigateur Firefox.)

## Chrome / Chromium
Chrome ne permet pas par défaut d'installer de module en dehors du Chrome Web Store. Si vous souhaitez absolument ajouter ce module, vous pouvez contourner celà en :
1. [téléchargeant le code source](https://framagit.org/framasoft/fun/proutify/-/archive/main/proutify-main.zip) puis en le décompressant,
2. ouvrant votre navigateur Chrome ou Chromium,
3. vous rendant sur `chrome://extensions`,
4. activant le "Mode développeur" (en haut à droite),
5. cliquant sur "Charger l'extension non-empaquetée",
6. sélectionnant le dossier contenant l'application

